# -*- coding: utf-8 -*-
# Copyright (C) Odoo Community Association (OCA)
# @author: Alexandre Fayolle
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).
{
    'name': 'Project members',
    'summary': 'Add members to projects',
    'version': '10.0.1.0.0',
    'category': 'Project',
    'depends': ['project'],
    'data': ['views/project.xml'],
    'license': 'AGPL-3',
    'author': 'Alexandre Fayolle <alexandre.fayolle@camptocamp.com>,'
               'Odoo Community Association (OCA)',
}
